package com.example.fp_papb_f;
import java.io.Serializable;

public class ToDoItem implements Serializable {
    private String List;
    private String key;

    public ToDoItem(){

    }
    public ToDoItem(String list) {
        List = list;
    }

    public String getList() {
        return List;
    }

    public void setList(String list) {
        List = list;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
}
