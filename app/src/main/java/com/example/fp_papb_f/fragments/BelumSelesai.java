package com.example.fp_papb_f.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.fp_papb_f.AdapterList;
import com.example.fp_papb_f.AddList;
import com.example.fp_papb_f.EditListActivity;
import com.example.fp_papb_f.ToDoItem;
import com.example.fp_papb_f.R;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class BelumSelesai extends Fragment {
    RecyclerView recyclerView;
    FloatingActionButton actionButton;
    ToDoItem toDoItemItem;
    DatabaseReference database = FirebaseDatabase.getInstance().getReference();
    AdapterList adapterList;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_belum_selesai, container, false);

        recyclerView = view.findViewById(R.id.recyclerView);
        actionButton = view.findViewById(R.id.floatingActionButton);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        actionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), AddList.class);
                startActivity(intent);
            }
        });

        adapterList = new AdapterList(new ArrayList<ToDoItem>(), getContext());
        recyclerView.setAdapter(adapterList);


        adapterList.setOnItemClickListener(new AdapterList.OnItemClickListener() {
            @Override
            public void onEditClick(int position) {
                editItem(position);
            }

            @Override
            public void onDeleteClick(int position) {
                deleteItem(position);
            }
        });

        tampilData();

        return view;
    }

    private void tampilData() {
        database.child("listBelumSelesai").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                ArrayList<ToDoItem> toDoItemIt = new ArrayList<>();
                for (DataSnapshot item : snapshot.getChildren()){
                    ToDoItem toDoItem = item.getValue(ToDoItem.class);
                    toDoItem.setKey(item.getKey());
                    toDoItemIt.add(toDoItem);
                }
                adapterList.setData(toDoItemIt);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                // Handle error
            }
        });
    }

    private void editItem(int position) {
        ToDoItem selectedItem = adapterList.getItem(position);
        Intent intent = new Intent(getActivity(), EditListActivity.class);
        intent.putExtra("selectedItem", selectedItem);
        startActivity(intent);
    }

    private void deleteItem(int position) {
        ToDoItem selectedItem = adapterList.getItem(position);
        String itemKey = selectedItem.getKey();
        database.child("listBelumSelesai").child(itemKey).removeValue();
        Toast.makeText(getContext(), "List Selesai", Toast.LENGTH_SHORT).show();
    }
}
