package com.example.fp_papb_f.fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.example.fp_papb_f.AdapterListSelesai;
import com.example.fp_papb_f.R;
import com.example.fp_papb_f.ToDoItem;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class Selesai extends Fragment {

    RecyclerView recyclerView;
    Button buttonclear;
    DatabaseReference database = FirebaseDatabase.getInstance().getReference();
    AdapterListSelesai adapterListSelesai;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_selesai, container, false);

        recyclerView = view.findViewById(R.id.recyclerView);
        buttonclear = view.findViewById(R.id.buttonclear);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        buttonclear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(requireContext());
                builder.setMessage("Apakah anda yakin ingin menghapus seluruh list?")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                hapusAllData();
                            }
                        })
                        .setNegativeButton("No", null)
                        .show();
            }
        });

        adapterListSelesai = new AdapterListSelesai(new ArrayList<ToDoItem>(), getContext());
        recyclerView.setAdapter(adapterListSelesai);

        tampilData();

        return view;
    }

    private void tampilData() {
        database.child("listSelesai").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                ArrayList<ToDoItem> toDoItemIt = new ArrayList<>();
                for (DataSnapshot item : snapshot.getChildren()){
                    ToDoItem toDoItem = item.getValue(ToDoItem.class);
                    toDoItem.setKey(item.getKey());
                    toDoItemIt.add(toDoItem);
                }
                adapterListSelesai.setData(toDoItemIt);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                // Handle error
            }
        });
    }

    private void hapusAllData() {
        database.child("listSelesai").removeValue()
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Toast.makeText(requireContext(), "List berhasil dihapus", Toast.LENGTH_SHORT).show();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(requireContext(), "Gagal menghapus list", Toast.LENGTH_SHORT).show();
                    }
                });
    }

}