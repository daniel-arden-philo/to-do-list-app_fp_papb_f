package com.example.fp_papb_f;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class EditListActivity extends AppCompatActivity {

    EditText etList;
    Button btnSave;
    DatabaseReference database;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_list);

        etList = findViewById(R.id.et_list);
        btnSave = findViewById(R.id.bt_simpan);
        database = FirebaseDatabase.getInstance().getReference();

        ToDoItem selectedItem = (ToDoItem) getIntent().getSerializableExtra("selectedItem");

        etList.setText(selectedItem.getList());

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String editedList = etList.getText().toString();

                if (editedList.isEmpty()) {
                    etList.setError("Masukkan list yang diedit");
                } else {
                    updateItem(selectedItem.getKey(), editedList);
                }
            }
        });
    }

    private void updateItem(String key, String editedList) {
        database.child("listBelumSelesai").child(key).child("list").setValue(editedList)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            Toast.makeText(EditListActivity.this, "List berhasil diupdate", Toast.LENGTH_SHORT).show();
                            finish();
                        } else {
                            Toast.makeText(EditListActivity.this, "Gagal mengupdate list", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }
}
