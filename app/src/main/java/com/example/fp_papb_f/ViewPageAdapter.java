package com.example.fp_papb_f;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import com.example.fp_papb_f.fragments.BelumSelesai;
import com.example.fp_papb_f.fragments.Selesai;

public class ViewPageAdapter extends FragmentStateAdapter {

    public ViewPageAdapter(@NonNull FragmentActivity fragmentActivity) {
        super(fragmentActivity);
    }

    @NonNull
    @Override
    public Fragment createFragment(int position) {
        switch (position) {
            case 0:
                return new BelumSelesai();
            case 1:
                return new Selesai();
            default:
                return new BelumSelesai();
        }
    }

    @Override
    public int getItemCount() {
        return 2;
    }
}
