package com.example.fp_papb_f;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.List;

public class AdapterListSelesai extends RecyclerView.Adapter<AdapterListSelesai.MyViewHolder> {
    private ArrayList<ToDoItem> listItem;
    private Context activity;
    DatabaseReference database = FirebaseDatabase.getInstance().getReference();

    public AdapterListSelesai(ArrayList<ToDoItem> list, Context activity) {
        this.listItem = list;
        this.activity = activity;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View viewItem = inflater.inflate(R.layout.itemhistory, parent, false);
        return new MyViewHolder(viewItem);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        final ToDoItem item = listItem.get(position);
        holder.tv_item.setText(item.getList());
    }

    @Override
    public int getItemCount() {
        return listItem.size();
    }

    public void setData(List<ToDoItem> data) {
        listItem.clear();
        listItem.addAll(data);
        notifyDataSetChanged();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        CardView cardList;
        TextView tv_item;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            tv_item = itemView.findViewById(R.id.tv_item);;
            cardList = itemView.findViewById(R.id.cardView);
        }
    }
}
