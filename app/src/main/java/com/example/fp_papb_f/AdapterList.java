package com.example.fp_papb_f;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.List;

public class AdapterList extends RecyclerView.Adapter<AdapterList.MyViewHolder> {
    private ArrayList<ToDoItem> listItem;
    private Context activity;
    DatabaseReference database = FirebaseDatabase.getInstance().getReference();
    private OnItemClickListener mListener;

    public interface OnItemClickListener {
        void onEditClick(int position);
        void onDeleteClick(int position);
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        mListener = listener;
    }

    public AdapterList(ArrayList<ToDoItem> list, Context activity) {
        this.listItem = list;
        this.activity = activity;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View viewItem = inflater.inflate(R.layout.item, parent, false);
        return new MyViewHolder(viewItem);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        final ToDoItem item = listItem.get(position);
        holder.tv_item.setText(item.getList());

        holder.btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mListener != null) {
                    int position = holder.getAdapterPosition();
                    if (position != RecyclerView.NO_POSITION) {
                        mListener.onEditClick(position);
                    }
                }
            }
        });

        holder.btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mListener != null) {
                    int position = holder.getAdapterPosition();
                    if (position != RecyclerView.NO_POSITION) {
                        String itemId = listItem.get(position).getKey();
                        database.child("listBelumSelesai").child(itemId).removeValue();
                        database.child("listSelesai").child(itemId).setValue(listItem.get(position));
                        notifyDataSetChanged();
                        mListener.onDeleteClick(position);
                    }
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return listItem.size();
    }

    public void setData(List<ToDoItem> data) {
        listItem.clear();
        listItem.addAll(data);
        notifyDataSetChanged();
    }

    public ToDoItem getItem(int position) {
        return listItem.get(position);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        CheckBox checkBox;
        CardView cardList;
        TextView tv_item;
        Button btnDelete;
        Button btnEdit;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            tv_item = itemView.findViewById(R.id.tv_item);;
            cardList = itemView.findViewById(R.id.cardView);
            btnDelete = itemView.findViewById(R.id.delete);
            btnEdit = itemView.findViewById(R.id.edit);
        }
    }
}
