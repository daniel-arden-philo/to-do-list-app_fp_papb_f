package com.example.fp_papb_f;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.FirebaseApp;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class AddList extends AppCompatActivity {
    EditText et_list;
    TextView tv_kembali;
    Button bt_tambah;
    DatabaseReference database = FirebaseDatabase.getInstance().getReference();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_list);

        et_list = findViewById(R.id.et_list);
        tv_kembali = findViewById(R.id.tv_kembali);
        bt_tambah = findViewById(R.id.bt_tambah);
        FirebaseApp.initializeApp(this);


        tv_kembali.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(AddList.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        });

        bt_tambah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String getList = et_list.getText().toString();

                if (getList.isEmpty()){
                    et_list.setError("Masukkan list baru");
                } else {
                    database.child("listBelumSelesai").push().setValue(new ToDoItem(getList))
                            .addOnSuccessListener(new OnSuccessListener<Void>() {
                                @Override
                                public void onSuccess(Void unused) {
                                    Toast.makeText(AddList.this,"List berhasil ditambahkan",Toast.LENGTH_SHORT).show();
                                    startActivity(new Intent(AddList.this,MainActivity.class));
                                    finish();
                                }
                            }).addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
                                    Toast.makeText(AddList.this,"Gagal menambahkan list",Toast.LENGTH_SHORT).show();
                                }
                            });
                }
            }
        });
        }
    }

